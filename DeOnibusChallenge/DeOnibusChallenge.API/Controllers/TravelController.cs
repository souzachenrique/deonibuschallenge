﻿using DeOnibusChallenge.API.Services.DTO;
using DeOnibusChallenge.Domain.Travel;
using DeOnibusChallenge.Domain.Travel.Interfaces;
using DeOnibusChallenge.Infrastructure.CrossCutting;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeOnibusChallenge.API.Controllers
{
    [Route("api/travels")]
    [ApiController]
    public class TravelController : _BaseController
    {
        private readonly LogHelper logHelper;
        private readonly ITravelService _travelService;

        private const string msgSave = "Dados salvos com sucesso.";

        public TravelController(ITravelService travelService)
        {
            logHelper = new LogHelper();
            _travelService = travelService;
        }   

        [HttpGet]
        public async Task<ActionResult<List<Travel>>> GetExceptFavorite([FromQuery] TravelFilterDTO travelFilterDTO)
        {
            try
            {
                logHelper.Information("Obtendo todas as viagens.");
                logHelper.Information("Filtros enviados {@TravelFilterDTO}", travelFilterDTO);
                return await _travelService.GetAll(false, travelFilterDTO.MaxPrice, travelFilterDTO.BusClass, travelFilterDTO.TravelPeriod);
            }
            catch (Exception e)
            {
                logHelper.Error(e, "Ocorreu um erro ao tentar obter todos as viagens.");
                logHelper.Debug(e, "Método: GetExceptFavorite, com o seguinte objeto de entrada: {@TravelFilterDTO}", travelFilterDTO);
                return VerifyException(e);
            }
        }

        [HttpGet("{favorite}")]
        public async Task<ActionResult<List<Travel>>> GetFavorite([FromQuery] TravelFilterDTO travelFilterDTO)
        {
            try
            {
                logHelper.Information("Obtendo todas as viagens favoritas.");
                logHelper.Information("Filtros enviados {@TravelFilterDTO}", travelFilterDTO);
                return await _travelService.GetAll(true, travelFilterDTO.MaxPrice, travelFilterDTO.BusClass, travelFilterDTO.TravelPeriod);
            }
            catch (Exception e)
            {
                logHelper.Error(e, "Ocorreu um erro ao tentar obter todos as viagens favoritas.");
                logHelper.Debug(e, "Método: GetFavorite, com o seguinte objeto de entrada: {@TravelFilterDTO}", travelFilterDTO);
                return VerifyException(e);
            }
        }

        [HttpPost("favorite")]
        public async Task<ActionResult> CreateFavorite([FromBody] List<string> objectIds)
        {
            try
            {
                logHelper.Information("Criando a(s) favorito(a).", objectIds);
                await _travelService.CreateFavorite(objectIds);
                return StatusCode(201, msgSave);
            }
            catch (Exception e)
            {
                logHelper.Error(e, "Ocorreu um erro ao tentar criar a(s) favorita(s).");
                logHelper.Debug(e, "Método: CreateFavorite, com o seguinte objeto de entrada: {@objectIds}", objectIds);
                return VerifyException(e);
            }
        }

        [HttpDelete("favorite")]
        public async Task<ActionResult> DeleteFavorite([FromBody] List<string> objectIds)
        {
            try
            {
                logHelper.Information("Removendo a(s) favorita(s).", objectIds);
                await _travelService.DeleteFavorite(objectIds);
                return StatusCode(204);
            }
            catch (Exception e)
            {
                logHelper.Error(e, "Ocorreu um erro ao tentar remover a(s) favorito(a).");
                logHelper.Debug(e, "Método: DeleteFavorite, o seguinte objeto de entrada: {@objectIds}", objectIds);
                return VerifyException(e);
            }
        }

        [AllowAnonymous]
        [HttpGet("[action]")]
        public IActionResult HealthCheck()
        {
            return Ok("DeOnibus - Microserviço de viagem no ar.");
        }
    }
}
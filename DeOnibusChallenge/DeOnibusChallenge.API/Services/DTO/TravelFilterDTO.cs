﻿using DeOnibusChallenge.Domain.Travel.Enums;

namespace DeOnibusChallenge.API.Services.DTO
{
    public class TravelFilterDTO
    {
        public float? MaxPrice { get; set; }
        public string BusClass { get; set; }
        public TravelPeriodEnum? TravelPeriod { get; set; }

    }
}

﻿using DeOnibusChallenge.Domain.Travel;
using DeOnibusChallenge.Domain.Travel.Enums;
using DeOnibusChallenge.Domain.Travel.Interfaces;
using DeOnibusChallenge.Infrastructure.CrossCutting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeOnibusChallenge.API.Services
{
    public class TravelService : ITravelService
    {
        private readonly LogHelper logHelper;
        private readonly ITravelRepository _travelRepository;

        public TravelService(ITravelRepository travelRepository)
        {
            logHelper = new LogHelper();
            _travelRepository = travelRepository;
        }

        public async Task<List<Travel>> GetAll(bool favorite, float? filterMaxPrice, string filterBusClass, TravelPeriodEnum? filterTravelPeriod)
        {
            logHelper.Information("Obtendo viagens e favoritas");
            var travels = await _travelRepository.GetAll();
            var favorites = await _travelRepository.GetFavorite();

            if (favorite)
                travels = travels.Where(x => favorites.Select(x => x.TravelId).Contains(x.ObjectId)).ToList();
            else
                travels = travels.Where(x => !favorites.Select(x => x.TravelId).Contains(x.ObjectId)).ToList();

            logHelper.Information("Filtrando as viagens de acordo com os filtros enviados.");
            if (filterMaxPrice.HasValue)
                travels = travels.Where(x => x.Price <= filterMaxPrice.Value).ToList();

            if (!string.IsNullOrEmpty(filterBusClass))
                travels = travels.Where(x => x.BusClass.Equals(filterBusClass)).ToList();

            if (filterTravelPeriod.HasValue && Enum.IsDefined(typeof(TravelPeriodEnum), filterTravelPeriod))
            {
                TimeSpan startHour = new TimeSpan(), endHour = new TimeSpan();

                switch (filterTravelPeriod)
                {
                    case TravelPeriodEnum.Madrugada:
                        startHour = new TimeSpan(0, 0, 0);
                        endHour = new TimeSpan(5, 59, 59);
                        break;
                    case TravelPeriodEnum.Manha:
                        startHour = new TimeSpan(6, 0, 0);
                        endHour = new TimeSpan(11, 59, 59);
                        break;
                    case TravelPeriodEnum.Tarde:
                        startHour = new TimeSpan(12, 0, 0);
                        endHour = new TimeSpan(17, 59, 59);
                        break;
                    case TravelPeriodEnum.Noite:
                        startHour = new TimeSpan(18, 0, 0);
                        endHour = new TimeSpan(23, 59, 59);
                        break;
                }

                travels = travels.Where(x => x.DepartureDate.TimeOfDay >= startHour && x.DepartureDate.TimeOfDay <= endHour).ToList();
            }

            if (travels.Any())
            {
                logHelper.Information("Setando o destaque do melhor preço e formantando os campos.");
                travels.First(x => x.Price == travels.Min(x => x.Price)).SetBestPrice();
                travels.ForEach(x => x.SetFormattedValue());
            }

            return travels.OrderBy(x => x.DepartureDate).ToList();
        }

        public async Task CreateFavorite(List<string> objectIds)
        {
            StringBuilder msgError = new StringBuilder();

            foreach (var item in objectIds)
                msgError.Append(await _travelRepository.CreateFavorite(item));

            new Travel().FavoriteActionIsValid(msgError.ToString());
        }

        public async Task DeleteFavorite(List<string> objectIds)
        {
            StringBuilder msgError = new StringBuilder();

            foreach (var item in objectIds)
                msgError.Append(await _travelRepository.DeleteFavorite(item));

            new Travel().FavoriteActionIsValid(msgError.ToString());
        }
    }
}
using AutoMapper;
using DeOnibusChallenge.API.Services;
using DeOnibusChallenge.Domain.Travel.Interfaces;
using DeOnibusChallenge.Infrastructure.CrossCutting;
using DeOnibusChallenge.Infrastructure.Database;
using DeOnibusChallenge.Infrastructure.Mapping;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;

namespace DeOnibusChallenge.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();

            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new AutoMapping());
            });

            IMapper mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
            services.AddDbContext<DatabaseContext>(options => { options.UseSqlServer(Configuration.GetConnectionString("LocalDB")); });
            services.AddTransient<ITravelService, TravelService>();
            services.AddTransient<ITravelRepository, TravelRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            LogHelper log = new LogHelper();

            if (Convert.ToBoolean(Configuration[$"AppSettings:LoggingInFile"]))
                log.InitializeFile(loggerFactory, Configuration[$"AppSettings:LogginFilePath"], Configuration[$"AppSettings:LogginFileExtension"]);
        }
    }
}

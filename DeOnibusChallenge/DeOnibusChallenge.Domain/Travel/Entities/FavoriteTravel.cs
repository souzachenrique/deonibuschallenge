﻿using System.ComponentModel.DataAnnotations;

namespace DeOnibusChallenge.Domain.Travel.Entities
{
    public class FavoriteTravel : _BaseEntity
    {
        [Required]
        public string TravelId { get; set; }
    }
}
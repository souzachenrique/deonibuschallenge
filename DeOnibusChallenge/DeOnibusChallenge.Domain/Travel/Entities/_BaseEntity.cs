﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace DeOnibusChallenge.Domain.Travel.Entities
{
    public class _BaseEntity
    {
        [Key]
        public int Id { get; set; }

        public bool RequiredFieldIsValid(object o)
        {
            foreach (PropertyInfo pi in o.GetType().GetProperties())
            {
                if (pi.GetCustomAttributes(typeof(RequiredAttribute)).Any())
                {
                    if (pi.PropertyType == typeof(string) && string.IsNullOrEmpty((string)pi.GetValue(o)))
                        return false;

                    if (pi.PropertyType == typeof(int) && (int)pi.GetValue(o) <= 0)
                        return false;

                    if (pi.PropertyType == typeof(decimal) && (decimal)pi.GetValue(o) <= 0)
                        return false;

                    if (pi.PropertyType == typeof(DateTime) && (DateTime)pi.GetValue(o) == DateTime.MinValue)
                        return false;
                }
            }

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibusChallenge.Domain.Travel.Enums
{
    public enum TravelPeriodEnum
    {
        Madrugada,
        Manha,
        Tarde,
        Noite
    }
}

﻿using DeOnibusChallenge.Domain.Travel.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeOnibusChallenge.Domain.Travel.Interfaces
{
    public interface ITravelRepository
    {
        Task<List<Travel>> GetAll();
        Task<List<FavoriteTravel>> GetFavorite();
        Task<string> CreateFavorite(string objectId);
        Task<string> DeleteFavorite(string objectId);
    }
}
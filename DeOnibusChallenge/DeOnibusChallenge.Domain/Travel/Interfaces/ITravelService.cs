﻿using DeOnibusChallenge.Domain.Travel.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DeOnibusChallenge.Domain.Travel.Interfaces
{
    public interface ITravelService
    {
        Task CreateFavorite(List<string> objectIds);
        Task DeleteFavorite(List<string> objectIds);
        Task<List<Travel>> GetAll(bool favorite, float? filterMaxPrice, string filterBusClass, TravelPeriodEnum? filterTravelPeriod);
    }
}

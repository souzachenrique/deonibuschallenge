﻿using DeOnibusChallenge.Domain.Travel.Entities;
using System;

namespace DeOnibusChallenge.Domain.Travel
{
    public class Travel
    {
        public bool BestPrice { get; private set; }

        public float Price { get; set; }

        public string FormattedPrice { get; private set; }
        public string Origin { get; private set; }
        public string ObjectId { get; private set; }
        public string BusClass { get; private set; }
        public string CompanyName { get; private set; }
        public string Destination { get; private set; }
        public string FormattedArrivalDate { get; private set; }
        public string FormattedDepartureDate { get; private set; }

        public DateTime CreatedAt { get; private set; }
        public DateTime UpdatedAt { get; private set; }
        public DateTime ArrivalDate { get; private set; }
        public DateTime DepartureDate { get; private set; }

        public void SetBestPrice()
        {
            BestPrice = true;
        }

        public void SetFormattedValue()
        {
            Price = (float)Math.Round(Price, 2);
            FormattedArrivalDate = ArrivalDate.ToString();
            FormattedDepartureDate = DepartureDate.ToString();
            FormattedPrice = Price.ToString("c2");
        }

        public void FavoriteActionIsValid(string msgError)
        {
            if (!string.IsNullOrEmpty(msgError))
                throw new TravelException(msgError);
        }

        public bool VerifyRequiredFieldsFavorite(FavoriteTravel favoriteTravel)
        {
            return favoriteTravel.RequiredFieldIsValid(favoriteTravel);
        }
    }
}
﻿using System;

namespace DeOnibusChallenge.Domain.Travel
{
    public class TravelException : Exception
    {
        public TravelException(string message) :
            base(message)
        { }
    }
}
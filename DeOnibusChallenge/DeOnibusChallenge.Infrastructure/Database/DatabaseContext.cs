﻿using DeOnibusChallenge.Domain.Travel.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeOnibusChallenge.Infrastructure.Database
{
    public class DatabaseContext : DbContext
    {
        #region Constructor
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {

        }
        #endregion

        #region Tables

        public virtual DbSet<FavoriteTravel> FavoriteTravel { get; set; }

        #endregion
    }
}

﻿using AutoMapper;
using DeOnibusChallenge.Domain.Travel;
using DeOnibusChallenge.Domain.Travel.Entities;
using DeOnibusChallenge.Domain.Travel.Interfaces;
using DeOnibusChallenge.Infrastructure.CrossCutting;
using DeOnibusChallenge.Infrastructure.Integration.DTO;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeOnibusChallenge.Infrastructure.Database
{
    public class TravelRepository : ITravelRepository
    {
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        private readonly DatabaseContext _dbContext;


        public TravelRepository(IMapper mapper, DatabaseContext context, IConfiguration configuration)
        {
            _mapper = mapper;
            _dbContext = context;
            _configuration = configuration;
        }

        public async Task<string> CreateFavorite(string objectId)
        {
            StringBuilder msgError = new StringBuilder();

            var fav = _dbContext.FavoriteTravel.FirstOrDefault(x => x.TravelId == objectId);

            if (fav != null)
                msgError.Append($"Favorito {objectId} já adicionado.");
            else
            {
                var favoriteTravel = new FavoriteTravel { TravelId = objectId };

                if (new Travel().VerifyRequiredFieldsFavorite(favoriteTravel))
                {

                    await _dbContext.FavoriteTravel.AddAsync(favoriteTravel);
                    await _dbContext.SaveChangesAsync();
                }
                else
                    msgError.Append("Campo(s) obrigatório(s) não enviado(s).");
            }

            return msgError.ToString();
        }

        public async Task<string> DeleteFavorite(string objectId)
        {
            StringBuilder msgError = new StringBuilder();
            var fav = _dbContext.FavoriteTravel.FirstOrDefault(x => x.TravelId == objectId);

            if (fav == null)
                msgError.Append($"Favorito {objectId} não encontrado.");
            else
            {
                _dbContext.FavoriteTravel.Remove(fav);
                await _dbContext.SaveChangesAsync();
            }

            return msgError.ToString();
        }

        public async Task<List<Travel>> GetAll()
        {
            var apiHelper = new APIHelper(_configuration["AppSettings:TravelAPIUrl"]);
            apiHelper.AddHeader(_configuration["AppSettings:APINameHeaderId"], _configuration["AppSettings:APIId"]);
            apiHelper.AddHeader(_configuration["AppSettings:APINameHeaderKey"], _configuration["AppSettings:APIKey"]);

            var travels = await apiHelper.Get<TravelIntegrationDTO>(_configuration["AppSettings:TravelMethod"]);
            return _mapper.Map<List<TravelResponse>, List<Travel>>(travels.lstTravel);
        }

        public async Task<List<FavoriteTravel>> GetFavorite()
        {
            return await _dbContext.FavoriteTravel.ToListAsync();
        }
    }
}
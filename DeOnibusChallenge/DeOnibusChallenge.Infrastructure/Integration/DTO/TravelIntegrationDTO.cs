﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DeOnibusChallenge.Infrastructure.Integration.DTO
{
    public class TravelIntegrationDTO
    {
        [JsonProperty("results")]
        public List<TravelResponse> lstTravel { get; set; }
    }
    
    public class TravelResponse
    {
        public string objectId { get; set; }
        public Company Company { get; set; }
        public string Origin { get; set; }
        public string Destination { get; set; }
        public TraveIntegrationlDate DepartureDate { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public float Price { get; set; }
        public string BusClass { get; set; }
        public TraveIntegrationlDate ArrivalDate { get; set; }
    }

    public class Company
    {
        public string Name { get; set; }
    }

    public class TraveIntegrationlDate
    {
        public string __type { get; set; }
        public DateTime iso { get; set; }
    }
}
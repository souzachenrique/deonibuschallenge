﻿using AutoMapper;
using DeOnibusChallenge.Domain.Travel;
using DeOnibusChallenge.Infrastructure.Integration.DTO;
using System;

namespace DeOnibusChallenge.Infrastructure.Mapping
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<TravelResponse, Travel>()
                .ForMember(t => t.CompanyName, d => d.MapFrom(o => o.Company.Name))
                .ForMember(t => t.ArrivalDate, d => d.MapFrom(o => o.ArrivalDate.iso))
                .ForMember(t => t.DepartureDate, d => d.MapFrom(o => o.DepartureDate.iso));
        }
    }
}
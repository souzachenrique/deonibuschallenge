using DeOnibusChallenge.Domain.Travel;
using DeOnibusChallenge.Domain.Travel.Entities;
using System;
using Xunit;

namespace DeOnibusChallenge.Test.Domain 
{
    public class TravelTest
    {
        [Fact]
        public void SetBestPrice()
        {
            Travel travel = new Travel();
            var ev = true;

            travel.SetBestPrice();

            Assert.Equal(ev, travel.BestPrice);
            Assert.NotEqual(!ev, travel.BestPrice);
        }

        [Fact]
        public void NotBestPrice()
        {
            Travel travel = new Travel();
            var ev = true;

            Assert.NotEqual(ev, travel.BestPrice);
        }

        [Fact]
        public void VerifyFavoriteTravel()
        {
            bool isValid;
            Travel travel = new Travel();
            FavoriteTravel ft = new FavoriteTravel { TravelId = "123456" };

            var ev = true;

            isValid = travel.VerifyRequiredFieldsFavorite(ft);

            Assert.Equal(ev, isValid);
            Assert.NotEqual(!ev, isValid);
        }

        [Fact]
        public void VerifyRequiredFavoriteTravel()
        {
            bool isValid;
            Travel travel = new Travel();
            FavoriteTravel ft = new FavoriteTravel();

            var ev = true;

            isValid = travel.VerifyRequiredFieldsFavorite(ft);

            Assert.NotEqual(ev, isValid);
        }

        [Fact]
        public void FavoriteActionInvalid()
        {
            Travel travel = new Travel();

            Assert.Throws<TravelException>(() => travel.FavoriteActionIsValid("Tem que dar a exce��o"));
        }
    }
}

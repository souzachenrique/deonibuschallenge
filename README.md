# DeÔnibus

## Teste DotNet 😃

Dividimos o teste em 2 avaliações (FullStack e Backend), de acordo com a vaga que vocês estão se candidatando:

1. Teste FullStack

2. Teste Backend

Mobelo interativo (*navegue pelos frames e interaja com o modelo*): [https://xd.adobe.com/view/08aab9fe-e455-4a93-8a21-03fdb27de793-5df9/specs/](https://xd.adobe.com/view/08aab9fe-e455-4a93-8a21-03fdb27de793-5df9/specs/)

### 1. Teste FullStack - Tasks a serem desenvolvidas

- Fazer o fork deste repositório.
- Criar o projeto AspNet Core (preferencial) ou AspNet
- Consumir método de viagens na nossa api (mais abaixo na documentação)
- Listar viagens
    - Destacar viagem com melhor preço
    - Exibir lista ordenada por horário de partida
    - Formatação de valor e data
- Montar filtros levando em conta:
    - Classes disponíveis
    - Saída (Madrugada (00h:00 - 5h:59), Manhã (06h:00 - 11:59), Tarde (12h:00 - 17:59) e Noite (18h:00 - 23:59))
    - Preço máximo até R$ X
- Permitir escolher múltiplas viagens para depois adicionar todas à lista
- O menu lateral deve listar o resumo das viagens favoritas selecionadas
- Ao confirmar, fazer o envio da requisição ao backend e tratamento de sucesso ou erro. Registrar as informações em alguma das opções abaixo:
	a. Banco de dados local (preferencial)
	b. Arquivo
- Recarregar a tela removendo as passagens favoritas da listagem principal e fazendo a listagem delas no menu lateral
- Ao finalizar, subir apenas no seu próprio repositório e nos passar o link. **Não fazer pull request ou commit diretamente neste repositório**. [Como fazer o fork.](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html)

#### Teste FullStack - Informações importantes

- Alguns elementos na tela possuem ação de clique, mostrando como seria o fluxo de ação do usuário 🧐

- A adição das passagens a uma lista de favoritos deve ser feita somente ao clicar o botão *confirmar favoritos*, beleza?

- Backend: Injeção de dependência e métodos async

- Cá entre nós, se conseguir fazer uma versão responsiva vai ser massa!

- GL HF, ENJOY!  🤟

### 2. Teste Backend - Tasks a serem desenvolvidas

- Fazer o fork deste repositório.
- Criar o projeto API AspNet Core (preferencial) ou AspNet
- Método de listar viagens Consumir método de viagens na nossa api (mais abaixo na documentação)
	- Listar viagens
		- Destacar viagem com melhor preço
		- Exibir lista ordenada por horário de partida
		- Formatação de valor e data
	- Não exibir passagens favoritas que foram gravadas, pois estas aparecerão em uma lista a parte
	- Preparar filtros levando em conta:
		- Classes disponíveis
		- Saída (Madrugada (00h:00 - 5h:59), Manhã (06h:00 - 11:59), Tarde (12h:00 - 17:59) e Noite (18h:00 - 23:59))
		- Preço máximo até R$ X	
- Método para retornar viagens favoritas
- Metodo de gravar viagens favoritas, receber as viagens selecionadas e fazer tratamento de sucesso ou erro. Registrar as informações em alguma das opções abaixo:
	a. Banco de dados local (preferencial)
	b. Arquivo
- Método para remover viagens favoritas

#### Teste Backend - Informações importantes

- Estruturação da Api, com injeção de dependência e métodos async

- Padrão DDD

- Log das requisição, debug, erro, etc...

- Salvar os requests do Postman para testarmos o projeto na entrega

- Cá entre nós, se conseguir fazer uma versão responsiva vai ser massa!

- GL HF, ENJOY!  🤟

### Exemplo do Request 

Exemplo funcional no Postman: [DeOnibus-Public-Boilerplate.postman_collection.json](DeOnibus-Public-Boilerplate.postman_collection.json)

Dados da requisição: GET ROUTES

URL: https://4jehkg0izj.execute-api.us-east-1.amazonaws.com/stage-v0/route
Method: GET

Headers

- X-Parse-Application-Id: LtD1wBDjTJB7CcuF3hNRNmvRI9CQpozYzN7jIxfA

- X-Parse-REST-API-Key: 1TMvuvt9n2qHCqdQ8qpLw7DX6wUQpq2zhq0OGTvp

- content-type: application/json;

Response

- Success Status: 200 OK

- Body: JSON com uma lista de objetos.


---

### Considerações do desenvolvimento do teste

A arquitetura do projeto foi desenvolvida com base em um padrão que a Microsoft criou para microsserviços orientados a DDD (https://docs.microsoft.com/pt-br/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/ddd-oriented-microservice).

Foi utilizado o LocalDB para armazenamento dos favoritos, o script para criação do banco e tabela está na raiz da pasta DeOnibusChallenge.

Os requests do Postman, para testes das API's, estão no mesmo arquivo disponibilizado com o exemplo.

Os Logs estão sendo armazenados em um arquivo que é gerado dentro da pasta DeOnibusChallenge.API, essa geração é configurada pelo appsettings.json.
